# Blog de Movimiento Vital

## Blog Handbook

Este documento es el manual del blog. Aquí se encuentra la información
relevante a la creación de contenidos y su distribución.

## Contenido

1. [Introducción](#introducción)
2. [Creación de contenido](#creación-de-contenido)
  * [Proceso de escritura](#proceso-de-escritura)

## Introducción

Un blog es el medio de comunicación mediante el cual se pueden exponer
diversos temas. Las principales razones por las que Movimiento Vital cuenta
con su propio blog son:

* Dar a conocer los beneficios que trae consigo el cuidado de la salud desde
el punto de vista de la fisioterapia.
* Proporcionar información que genere una cultura de prevención.
* Demostrar el conocimiento con el que cuenta el equipo de Movimiento Vital.
* Ser un lugar donde diversos profesionistas puedan exponer sus propias
opiniones a traves de colaboraciones.
* Ofrecer información oportuna para que otros profesionistas puedan mantener
una educación continua.

## Creación de contenido

Es importante considerar que un buen artículo tiene la capacidad de llegar a
una audiencia grande y generar interes sobre el tema que se trate. Con un buen
escrito se crea la oportunidad de resaltar los beneficios que produce la
fisioterapia.

Los temas que se encuentren en el blog deben de tener relación con los
pacientes y profesionistas que lo lean. Algunas de estas personas poseen
conocimientos técnicos del área, pero esto no es la norma. El público puede
incluir a deportistas amateur, personas con dolores físicos, personas que
desean mejorar sus condiciones de salud, personas con curiosidad de problemas
específicos, profesionistas y estudiantes de fisioterapia. Antes de escribir
un artículo es necesario definir a que audiencia se busca alcanzar.

Algunos de los temas que pueden ser de interes son:

* Movimiento Vital
  * Información de eventos realizados por Movimiento Vital.
  * Noticias relevantes del centro.

* Casos de pacientes.
  * Problemas comunes.
  * Prevención de lesiones.
  * Tips y recomendaciones respecto al cuidado del cuerpo.

* Información especializada.
  * Casos de estudio.
  * Desarrollo de nuevas técnicas de terapia.
  * Lesiones poco comunes.
  * Empoderamiento profesional.
  * Metodologías interdisciplinarias.

### Proceso de escritura

Para los miembros de Movimiento Vital el proceso de escritura seguirá los
siguientes pasos:

1. Elección del tema.
2. Definir la audiciencia meta.
3. Crear la estructura básica del artículo. Definir cuales son los puntos
clave a tratar.
4. Crear una entrada en el calendario de publicaciones con el tema y la
estructura básica.
5. Establecer una fecha de entrega.
6. Escribir el artículo siguiendo las guías de estilo y las técnicas
de escritura establecidas.
7. Enviar el artículo al equipo de marketing para su revisión.
8. La versión revisada debe de tener el visto bueno por algún miembro de
Movimiento Vital distinto al autor
9. El artículo será publicado.
10. El equipo de marketing lo difundirá en redes sociales.

Para los escritores que no pertenezcan a Movimiento Vital el proceso de
escritura seguirá los siguientes pasos:

1. Elección del tema.
2. Envío de un borrador.
3. El equipo de marketing analizará la propuesta.
4. Si el tema se considera relevante se le notificará al colaborador para que
continue mejorando el artículo y se fijará una fecha de entrega.
5. El colaborador debe escribir el artículo siguiendo las guás de estilo y
las técnicas de escritura establecidas.
6. Enviar el documento final al equipo de marketing para una primera revisión.
7. El equipo de Movimiento Vital hará una revisión técnica.
8. El artículo será publicado.
9. El equipo de marketing lo difundirá en redes sociales.

**Importante**: La última sección del artículo debe contener información sobre
el autor, la cual se incluirá dentro del mismo. Esta parte debe ser escrita por
el colaborador a modo de presentación para que los lectores sepan quien es y
porque razón Movimiento Vital considera valiosa su opinión.